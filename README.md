# HouseholdBook

HouseholdBookは複数人で使用することを目的とした家計簿アプリです。  
現在はローカルで使用することを目的としています。  

# 使い方

1.支出入力  
データの入力ができます。  
日付、メモ、金額、タグを入力・選択して入力完了ボタンを押すと入力完了です。  

![MainActivity](https://lh3.googleusercontent.com/8ayrfcbLVbAYP021svMusuHI5Sl_AqpHuUoXeznh5ITUvsjFphBypmRzcHx_CHxjIDPq23BzHErbErlOlhpWBUXim-BXhnF1MRzutJpsPPgqD5FXQm53pCaE4ablz7JwCPtLQSWw_IcXt_xqKCi_Kc1ogp0CZJa3Wnk7kPtioHAZ72NPitOEK0Ph0tcmDYZgLmS3n_VS8MrrSo3p6SAQGCVGYfkrmiFmR4CN0Ps3hBNV-oFG3Fk9r0THHDrh6PaIspQEkTHgThD4TsOn2g5Mk7M51v8UZ-1gi_yLfYY4hSK8pQxt1C9ygIontD0wfcJzB0bM3Tu9-Pe9bBs5n1qIRuM3F4-fbBc8DzsOuCDOnhC23ug40YTcsNNKwRYwdTccZqaZ-6H4WwGr2b6Q2vnhNIqqJUsH57SYrLUJ0UulRo7Mfo-f_hgdFF9jWi-fBLjxsEU7OZz1EovFMHmQk-9he0DIk96lCyyFRMn13oxFNZfSJC_zF9Gni4pQkygDHESNKxoZfPDNF1wJ-cdNw_hq2yEnz9HCtn6TS0ZSYPIvIOQ9PzAXa_qxL8O3LaAzRZ0SVQOo-IL4gOZVDpx2N04AKht23gXmvuUEwyKOjxa_JAHbkrcme6rNMqWQO0cFv9_js9jl89Ya8uQPP9_IH_9RRwPtuEq7tAKOPnv8SRyXiGbSFf6R6jeZ-0QnyAIK-IMrQX-vuN27DFIQoV5lMPDoJpoeeIQ4eKGOCeH7-DsnAThMWq3bE6vN7dlKzkoeGA=w225-h400-no?authuser=0)

2.カレンダー  
カレンダーの日付をタップすると下部にその日の支出情報が一覧表示されます。  

![CalendarActivity](https://lh3.googleusercontent.com/6NN1qwzYqCnu-at_ZhepUQxTrkKGBzvoGVz-w9h5vvffcs5w4djk3WT1stnoaZK_uXduo4YjO44tjGiYQ4gigm4LhaDhq2revImRwF92fIPe-oCn0gvU2y56BZ71F62U4p3J78Bucw4rNG8YSq6OmJKnCznmzRAtIJr5GyFTlwGZcQQRwnXkLLLVZ6hOgF8bs6t6d8XsaYwRYW4YX-d-nQhr_x8mxltrXkowiSGq8wit2rKjEP75e_3nsv97AIM8Z2ubLw-S-4Otzy631dR-6VpwEiq4dA2EdNNAZEiYti3jIv72jAxipW8sCUHqzAQ5E_t-8_QcwCExmoL7F1Wj8ypOSA8sbmOi9kpzC0x_h6L7a6XCpvjtjVbGuNhpAGndsCmEAnIJLAUYB9z6pCdvah0-Au8It5n5fG2yy9jnlF5z1XX-8nZoFk8QeRzhin5l_vSfnxCDpABW03nPYzSiciwEWfRuSzdEKiEZC_TZvKzqJhpMMntO5tCaW-HRxpsU5Y9wmqu0FoHO2r8qh6R6f4K9qsdWAv4mzdhQuHxfU9jQtp21bwgYQQQC41y-MHmI15YRb38KsPSNRXKP9-Psq63DQTk4HsfCRv5jAcTl1XVaU6N7vAHXpTJkUyIfK7jmvKMADSg2GNBcdXajsho1oBBtphEuEjVxSWZyZsXOY05eUYoPScWWX6IDDzqiZaaTZTdTvCaBQl7V85KnAN-TDVpkCViklrlcfKhMcdWMjZHM72nozG4AWElRCmKTLA=w225-h400-no?authuser=0)

3.レポート  
一か月の支出情報をタグごとに円グラフで見ることができます。  
下部には支出情報がタグごとに一覧表示されます。  

![ReportActivity](https://lh3.googleusercontent.com/aKjW7hQl7Nf788mvC_x9Dqk5KkSWDsuwOmup3TqiPRbjfhpU6T956hvEUi_njBWNi_TOdDNkSPH_IK9_v9ggt8YM34dcNElCHRSaxXhUPiMppnuZj5m9rXjPJoW8fWPftJhDw5MzxRTklWQKir6FjiK-AEfRyPbEoFnu10-tZ7JCBeRBUeAMsN55LrxlXvA4DdGg5wWoRF9rxPd5s4N5_yhNVqGjbKP9bHlRCITn0kyQANZhaq1DR57AiQQiF8GPXV-s9O-7VhPp2uOZZmzZeOYnLX7ETG7wak8ZD_Boo15piVUbbf7-VB7nV16DWbIJHjv-AAT5GSa3q7-t9LgNZHSk1roUZy6ejVp5zJnKnh0w_xGejW4v4SM1fZguI3WRlBxjhqVT8MAw7R7rFOALqVQbtp1Y05kCfiJChY43Qlteby7mOCI_O1JK_4y0aLMuVGhen92nlQ8VH2_8JZ2WnO8wL5ip02ixwB8iyKYeaAhwMCKIqU34fF26RdQloUp1yknOZ25IJUD_H4hWRwknfWri60XZv9n7dA-kN1QZhjdsbUAoTp5qH_xMeWujGWD7QQDlY2_YcsFu7TXay672u3zJIIgAeOqeBqTJvsaiME3VXMzJWW5D9YoH3TvIrnOyfRKfSRfUMdXd-nZGyl_Z97mDYms4aOF4rC_crTSo4_k6EsIGCyEGQqLX4irAtxYZcNe-Ju-y8lXJZLhSulHPmGBOJWm6sPt4sVjshptnTtO_xxk__NkQRfLMy2zxSw=w225-h400-no?authuser=0)

4.その他  
ヘルプ画面や固定支出画面への遷移、ユーザー名の設定ができます。  

![OthersActivity](https://lh3.googleusercontent.com/LWFYj_GqcdL2CJWCYq-jCWjCrXUbIUSoK6smXDNEonxiL5_l8R-o9z9ywqBIAWIAvrAC72MPyaZD95oBS682xD1ln9eskZhuQ5kozynmdE-mHMWQqb8Jf_fA54W2Fs1bxZd4_FEVRguMTP15Lvp3_Sf1bctpDRj-pgaqX-lpfRomwKBFVSFfhbo_cBWFtnmaichYNGsXpj3pQ2YEeYJtdNjD-WaRv-jel6aEIhBkLc9Lih-bB7HHpcWfJoLtr6gAss3Rs_G57aEq9Yao2ubNB44Eu6gRbjLpXL5AJrrLZsJWB5y0CCIJrbMSJPIbr0Hh9b6BpOtW56K_v5a6jKhlLd69f5Tm-GI9rpZbNpxLRjvd1pSt6NEEflNYMj3ASiYaP6Ko5ZZb-idFMVsyAMIYE_HE_wC9coduKvTft3OlmVQeOEK-bTiWi2cy92e7oVWE0DMt3YK6CouiJWvzKQlIWivqMUWEzhsplOTjlfq0y_y9Ns76xztdZvHdd6RpwkA9HH3F0PtUDsCN9VSyZWxTu0YLFk0AxV2OxJoNK6EVUba8w0ZZGx02ZWSfE_e89BRUJNiJB5yre1qsM11KrR4U--U1fDdWlrW1gZ9QdJNkMqkChJI7yXsJvbAASWtTim-S89q7OuH8PzXOMgmzgcELGzQ7zoccwWjlrSKjhLpPxjYi1fCRYeKSxqjpUQ23bx8LZ4qgqOk856ME5IbGJXcdx4ZMbOkWByAGSFw3bc5uqKMVUq_IdlEWx1kxa0Y1gA=w225-h400-no?authuser=0)

# 今後の実装予定

・データのリセット  
・固定支出の設定  
・カレンダー・レポート画面における一か月の総支出額の表示  
・カレンダー画面からのデータの編集、削除  
・ヘルプ画面の内容の記載  

# 開発環境・使用技術

・Windows 10  
・Android Studio Bumblebee | 2021.1.1 Patch 2  
・java version 1.7.0_80  
・Firebase Firestore database  
・Sourcetree version 3.4.5.0  
